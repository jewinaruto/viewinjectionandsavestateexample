This is an example project that shows how to inject views and how to save the state of your fields using SlothFramework.

Here we have a capture of the app:

![example_app.png](https://bitbucket.org/repo/4MEAyj/images/935630525-example_app.png)


This example uses the [SlothFramework](https://bitbucket.org/jewinaruto/slothframework)

![sloth_logo-03.png](https://bitbucket.org/repo/K5Eody/images/386427969-sloth_logo-03.png)