package es.slothdevelopers.slothframework.example;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import es.slothdevelopers.slothframework.activity.SlothActionBarActivity;
import es.slothdevelopers.slothframework.annotations.InjectView;
import es.slothdevelopers.slothframework.annotations.SaveState;

public class MainActivity extends SlothActionBarActivity implements View.OnClickListener {
    @SaveState
    int counter = 0;

    @InjectView(R.id.counter)       TextView counterCount;
    @InjectView(R.id.increaseCount) Button increaseCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        increaseCount.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        counter++;
        updateCounterCount();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateCounterCount();
    }

    private void updateCounterCount(){
        counterCount.setText(String.valueOf(counter));
    }
}
